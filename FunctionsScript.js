import {randomUserMock, additionalUsers} from "./FE4U-Lab3-mock.js";
import countriesData from './CountriesToContinents.json' assert {type: 'json'};

export var courses = ["Mathematics", "Physics", "English", "Computer Science", "Dancing", 
"Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"];
export var usersData = [];
export var countriesToContinents = {};
var rawDataCountries = countriesData;


function FormatCountriesAndContinents()
{
    for(let i = 0;i<rawDataCountries.length;++i)
    {
        if(countriesToContinents[rawDataCountries[i].region]==undefined)
        countriesToContinents[rawDataCountries[i].region] = [];
        countriesToContinents[rawDataCountries[i].region].push(rawDataCountries[i].country);
    }
}
FormatCountriesAndContinents();

function randomIntegerGenerator(min, max)
{
    return Math.floor(Math.random() * (max - min)) + min;
}

export function ValidateUserData(user)
{
  
    if(!_.isString(user.full_name)|| _.isNull(user.full_name)|| _.upperFirst(user.full_name) !=  user.full_name)
        return false;

    if(!_.isUndefined(user.gender))
    {
        if(!_.isString(user.gender) || _.isNull(user.gender) || _.upperFirst(user.gender) !=  user.gender)
            return false;
    }
    else return false;

    if(!_.isNull(user.note) && _.trim(user.note) != "" && !_.isUndefined(user.note))
    {
        if(!_.isString(user.note) || _.upperFirst(user.note) !=  user.note)
            return false;
    }
    else return false;

    if(!_.isUndefined(user.state))
    {
        if(!_.isString(user.state) || _.isNull(user.state) || _.upperFirst(user.state) !=  user.state)
            return false;
    }
    else return false;

    if(!_.isUndefined(user.city))
    {
        if(!_.isString(user.city) || _.isNull(user.city) || _.upperFirst(user.city) !=  user.city)
            return false;
    }
    else return false;

    if(!_.isUndefined(user.country))
    {
        if(!_.isString(user.country) || _.isNull(user.country) || _.upperFirst(user.country) !=  user.country)
            return false;
    }
    else return false;

    if(!_.isUndefined(user.age))
    {
        if(!_.isNumber(user.age))
            return false;
    }
    else return false;

    if(!_.isUndefined(user.email))
    {
        let emailFormat = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
        if(!_.isString(user.email) || !emailFormat.test(user.email))
           return false;
    }
    else return false;

    return true;
}

var currentUserID = 0;

export function FormatUserData(userData)
{
    let newFormatUser = {};
    //tranforming randomUsersMockData to correct format

    newFormatUser.gender = _.upperFirst(userData.gender);
    newFormatUser.title = userData.name.title;
    newFormatUser.full_name = (userData.name.first + " " + userData.name.last).toString();
    newFormatUser.city = userData.location.city;
    newFormatUser.state = userData.location.state;
    newFormatUser.country = userData.location.country;
    newFormatUser.postcode = userData.location.postcode;
    newFormatUser.coordinates = userData.location.coordinates;
    newFormatUser.timezone = userData.location.timezone;
    newFormatUser.email = userData.email;
    newFormatUser.b_date = userData.dob.date;
    newFormatUser.age = userData.dob.age;
    newFormatUser.phone = userData.phone;
    newFormatUser.picture_large = userData.picture.large;
    newFormatUser.picture_thumbnail = userData.picture.thumbnail;
    
    newFormatUser.id = currentUserID++;
    newFormatUser.favourite = (randomIntegerGenerator(0,10)%3 == 0);
    newFormatUser.course = courses[randomIntegerGenerator(0,courses.length)];
    newFormatUser.bg_color = "#C2CED3";
    newFormatUser.note = "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quam aut veniam eveniet itaque, et debitis.";
    if(ValidateUserData(newFormatUser))return newFormatUser;
    return null;
}

export function FilterUsers(dataInput, parameters)
{
    let result = _.filter(dataInput, function(user){
        let currentComparison = true;
        for(let j = 0;j<parameters.length;++j)
        {
            let currentParameterValueComparison = true;
            if(typeof(parameters[j].range) != "undefined")
            {
                currentParameterValueComparison &= parameters[j].sat_cond ?
                (parameters[j].range[0]<=user[parameters[j].key] &&
                user[parameters[j].key]<=parameters[j].range[1]) :
                !(parameters[j].range[0]<=user[parameters[j].key] &&
                user[parameters[j].key]<=parameters[j].range[1]);
            }
            else
            {
                if(!parameters.sat_all_cond)currentParameterValueComparison = false;
                for(let k = 0;k<parameters[j].values.length;++k)
                {
                    if(parameters.sat_all_cond)
                    {
                        currentParameterValueComparison &= parameters[j].sat_cond ?
                        (user[parameters[j].key] == parameters[j].values[k]) :
                        !(user[parameters[j].key] == parameters[j].values[k]);
                    }
                    else 
                    {
                        currentParameterValueComparison |= parameters[j].sat_cond ?
                        (user[parameters[j].key] == parameters[j].values[k]) :
                        !(user[parameters[j].key] == parameters[j].values[k]);
                    }
                }
            }
            currentComparison &= currentParameterValueComparison;
        }
        return currentComparison;
    });
    return result;
}

export function FilterUsersPercentage(dataInput, parameters)
{
    let filterResult = FilterUsers(dataInput, parameters);
    return (filterResult.length / dataInput.length) * 100;
}

export function SearchUserBy(dataInput, parameters)
{
    let result = _.find(dataInput, _.matches(parameters));
    return _.isUndefined(result)? null : result;
}

export function SortUsers(inputData, keyParameters, isAsce = true)
{
    if(keyParameters.length==0)return;
    let valueOrder = isAsce ? 'asc': 'desc';
    let orders = _.times(keyParameters.length, _.constant(valueOrder));
    console.log(orders);
    inputData = _.orderBy(inputData, keyParameters, orders);
}


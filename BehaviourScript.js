import { FilterUsers } from "./FunctionsScript.js";
import { SortUsers } from "./FunctionsScript.js";
import { FilterUsersPercentage } from "./FunctionsScript.js";
import { SearchUserBy } from "./FunctionsScript.js";
import { ValidateUserData } from "./FunctionsScript.js";
import { countriesToContinents } from "./FunctionsScript.js";
import { FormatUserData } from "./FunctionsScript.js";
import {courses} from "./FunctionsScript.js";

var usersData = [];

function RemoveAllChildren(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

//Random user API Fetch Functions
function GetRandomUsersFetch(quantity)
{
    let url = `https://randomuser.me/api/?results=${quantity}`;
    return fetch(url)
    .then((response) => response.json())
    .then((data) => data.results)
    .catch((err) => {
        console.log(err);
     }) 
    
}

//User generation
document.getElementById("NextUsersButton").addEventListener("click", async function()
{
    let RawDataNewUsers = await GetRandomUsersFetch(10);
    for(let i = 0;i<RawDataNewUsers.length;++i)
    {
        usersData.push(FormatUserData(RawDataNewUsers[i]));
    }
    UpdateCurrentTeachers();
    DisplayCurrentTeachers();
    UpdateFavouriteTeachers();
    DisplayFavouriteTeachers();
    DisplayCharts();
});
//

var rawData = await GetRandomUsersFetch(50);
for(let i = 0;i<rawData.length;++i)
{
    let newFormat = FormatUserData(rawData[i]);
    if(newFormat!=null)
        usersData.push(FormatUserData(rawData[i]));
}

//Uploading data from saved json file
async function UploadUserDatabaseData()
{
    var usersDatabase = await fetch("db.json", ).then((response) => { 
        return response.json().then((data) => {
            return data;
        }).catch((err) => {
            console.log(err);
        }) 
        });
        for(let i = 0;i<usersDatabase.database.length;++i)
            usersData.push(usersDatabase.database[i]);
}
await UploadUserDatabaseData();


var mapIsOpen = false;
const ToggleMap = document.getElementById("toggleMapLink");
ToggleMap.addEventListener("click", function(e)
    {
        mapIsOpen = ~mapIsOpen;
        TeacherInfoMap.style.display = mapIsOpen ? "block" :"none";

    });

const TeacherInfoPicture = document.getElementById("teacherInfoPicture");
const TeacherInfoName = document.getElementById("fullNameTeacherInfo");
const TeacherInfoCity = document.getElementById("cityTeacherInfo");
const TeacherInfoCountry = document.getElementById("countryTeacherInfo");
const TeacherInfoAge = document.getElementById("ageTeacherInfo");
const TeacherInfoSex = document.getElementById("sexTeacherInfo");
const TeacherInfoEmail = document.getElementById("emailTeacherInfo");
const TeacherInfoPhone = document.getElementById("phoneNumberTeacherInfo");
const TeacherInfoNote = document.getElementById("commentTeacherInfo");
const TeacherInfoMap = document.getElementById("mapImageInfo");
const TeacherInfoFavouriteButton = document.getElementById("setFavouriteButton");
const TeacherInfoDaysTillBirthday = document.getElementById("daysLeftBirthdayLabel");
const fullCardContent = document.getElementById("fullCardContent");

var currentSelectedTeacher = null;
var currentTeachers = usersData;

function DisplayCurrentTeachers()
{
    RemoveAllChildren(document.getElementById("topTeachersDisplayPanel"));
    for(let i = 0;i<currentTeachers.length;++i)
    {
        DisplayCompactCardAtPanel(currentTeachers[i], "topTeachersDisplayPanel");
    }
}

//Teacher form. Add new teacher
var teacherForm = document.getElementById("teacherForm");
teacherForm.addEventListener("submit", function(event)
{
    event.preventDefault();
    let fullName = document.getElementById("fullNameInput");
    let specialty = document.getElementById("specialtyInput");
    let email = document.getElementById("emailInput");
    let phoneNumber = document.getElementById("phoneNumberInput");
    let country = document.getElementById("countryInput");
    let city = document.getElementById("cityInput");
    let birthdayDate = document.getElementById("BirthdayDateInput");
    let genderMale = document.getElementById("RadioButtonMaleInput");
    let backgroundColor = document.getElementById("backgroudColorInput");
    let comment = document.getElementById("noteInputField");

    let newUser = {};
    newUser = {
        ...newUser,
        full_name: fullName.value,
        course: specialty.value,
        email: email.value,
        phone: phoneNumber.value,
        country: country.value,
        city: city.value,
        b_date: birthdayDate.value,
        coordinates: {}
    };

    let dateValues = birthdayDate.value.split('-');
    let currentDate = new Date();
    let age = currentDate.getFullYear() - parseInt(dateValues[0]);
    if(currentDate.getMonth()<parseInt(dateValues[1]))
    {
        --age;
    }
    else if(currentDate.getMonth() == parseInt(dateValues[1]))
    {
        if(currentDate.getDay()<parseInt(dateValues[2]))--age;
    }
    let gender = genderMale.checked ? "Male" : "Female";
    let bg_color = backgroundColor.value;
    
    let note = comment.value.trim();
    
    newUser = {
        ...newUser,
        age: age,
        gender: gender,
        bg_color: bg_color,
        note: note
    };
    if(SearchUserBy(usersData, {full_name: newUser.full_name}) == null && ValidateUserData(newUser))
    {   

        let requestOptions = {
            method: 'GET',
          };
        let obj;
        fetch(`https://api.geoapify.com/v1/geocode/search?text=11%20${newUser.city}%2C%20${newUser.country}&apiKey=c4d7028bdaf64112a3320b300e0ff1e0`)
        .then(res => res.json())
        .then(data => {
            newUser.coordinates.latitude = data.features[0].bbox[1];
            newUser.coordinates.longitude = data.features[0].bbox[0];
         });
        usersData.push(newUser);
        
        fetch("http://localhost:3000/database",
        {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            },
            body: JSON.stringify(newUser)
        });

        UpdateCurrentTeachers();
        DisplayCurrentTeachers();
    }

});
//

document.getElementById("searchButton").addEventListener("click",function()
{
    let searchBoxValue = document.getElementById("searchTextBox").value;
    let overlay = document.getElementById("overlay");
    let popupInfo = document.getElementById("popupTeacherInfo");
    if(searchBoxValue != "")
    {
        let foundUserByFullname = SearchUserBy(usersData, {full_name: searchBoxValue});
        if(foundUserByFullname != null)
        {
            ChangePopupTeacherInfo(foundUserByFullname);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
        let foundUserByNote = SearchUserBy(usersData, {note: searchBoxValue});
        if(foundUserByNote != null)
        {
            ChangePopupTeacherInfo(foundUserByNote);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
        let foundUserByAge = SearchUserBy(usersData, {age: parseInt(searchBoxValue)});
        if(foundUserByAge != null)
        {
            ChangePopupTeacherInfo(foundUserByAge);
            document.getElementById("refToTeacherInfo").click();
            return;
        }
    }
});

var map = L.map('mapImageInfo').setView([0, 0], 13);

function ChangePopupTeacherInfo(teacher)
{
    TeacherInfoPicture.src = teacher.picture_large;
    TeacherInfoFavouriteButton.innerHTML = teacher.favourite ? "&starf;" : "&star;";
    TeacherInfoFavouriteButton.className = teacher.favourite ? "setFavouriteButtonTrue" : "setFavouriteButtonFalse";
    TeacherInfoName.innerHTML = teacher.full_name;
    TeacherInfoCity.innerHTML = teacher.city;
    TeacherInfoCountry.innerHTML = teacher.country;
    TeacherInfoAge.innerHTML = teacher.age;
    TeacherInfoSex.innerHTML = teacher.gender;

    let daysTillBirthday = DaysTillBirthday(teacher.b_date);
    if(daysTillBirthday>=365)TeacherInfoDaysTillBirthday.innerHTML = "Happy Birthday, " + teacher.full_name + "!";
    else TeacherInfoDaysTillBirthday.innerHTML = "Days till birthday: " + (daysTillBirthday + 1);

    fullCardContent.style.backgroundColor = teacher.bg_color;

    const emailRef = document.createElement("a");
    emailRef.href = "";
    emailRef.className = "emailLink";
    emailRef.innerHTML = teacher.email;
    RemoveAllChildren(TeacherInfoEmail);
    TeacherInfoEmail.append(emailRef);

    TeacherInfoPhone.innerHTML = teacher.phone;
    
    TeacherInfoNote.innerHTML = teacher.note;
    map.off();
    map.remove();
    map = L.map('mapImageInfo').setView([teacher.coordinates.latitude,teacher.coordinates.longitude], 13);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    let marker = L.marker([teacher.coordinates.latitude,teacher.coordinates.longitude]).addTo(map);
}

function СardClickHandler(elem, teacher)
{
    elem.addEventListener("click", function(e){
            currentSelectedTeacher = teacher;
            ChangePopupTeacherInfo(teacher);
        }
    );
}

TeacherInfoFavouriteButton.addEventListener("click", function()
{
    if(TeacherInfoFavouriteButton.className == "setFavouriteButtonTrue")
    {
        TeacherInfoFavouriteButton.className = "setFavouriteButtonFalse";
        TeacherInfoFavouriteButton.innerHTML = "&star;";
        currentSelectedTeacher.favourite = false;
    }
    else
    {
        TeacherInfoFavouriteButton.className = "setFavouriteButtonTrue";
        TeacherInfoFavouriteButton.innerHTML = "&starf;";
        currentSelectedTeacher.favourite = true;
    }
    RemoveAllChildren(document.getElementById("topTeachersDisplayPanel"));
    for(let i = 0;i<currentTeachers.length;++i)
    {
        DisplayCompactCardAtPanel(currentTeachers[i], "topTeachersDisplayPanel");  
    }
    favUsers = FavouriteTeachers();
    maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);
    DisplayFavouriteTeachers();
});

function DisplayCompactCardAtPanel(teacher, displayPanelID)
{
    const displayTeachers = document.getElementById(displayPanelID);

    const teacherCompactCard = document.createElement("div");
    teacherCompactCard.className = "teacherCompactCard";
    teacherCompactCard.id = "Teacher" + teacher.id;

    const pictureWrap = document.createElement("div");
    pictureWrap.className  = "pictureDivWrap";
    СardClickHandler(pictureWrap, teacher);
    
    const favourite = document.createElement("div");
    favourite.className = "isFavourite";
    favourite.innerHTML = "&starf;";
    favourite.style.visibility = teacher.favourite ? "visible" : "hidden";
    
    const anchorInfoPopup = document.createElement("a");
    anchorInfoPopup.href = "#popupTeacherInfo";
    
    const pictureDiv = document.createElement("div");
    pictureDiv.className = "pictureDiv";
    
    const initials = document.createElement("label");
    initials.className = "alternativeInitials";
    for(let i = 0;i<teacher.full_name.length;++i)
    {
        if(teacher.full_name[i].toUpperCase() ==  teacher.full_name[i] && teacher.full_name[i]!=" ")
            initials.innerHTML += teacher.full_name[i] + ".";
    }
    
    const teacherPicturePreview = document.createElement("div");
    teacherPicturePreview.className = "teacherPicturePreview";
    teacherPicturePreview.style.backgroundImage = "url('" + teacher.picture_large + "')";
    
    pictureDiv.append(initials);
    pictureDiv.append(teacherPicturePreview);
    
    anchorInfoPopup.append(pictureDiv);
    
    pictureWrap.append(favourite);
    pictureWrap.append(anchorInfoPopup);
    
    teacherCompactCard.append(pictureWrap);
    
    const fullNameDiv = document.createElement("div");
    fullNameDiv.id = "fullNameDiv";
    
    const fullNameLabel = document.createElement("label");
    fullNameLabel.id = "fullName";
    fullNameLabel.className = "teacherName";
    
    let names = teacher.full_name.split(' ');

    const spanFirstName = document.createElement("span");
    spanFirstName.innerHTML = names[0];
    
    const spanLastName = document.createElement("span");
    spanLastName.innerHTML = names[1];
    
    fullNameLabel.append(spanFirstName);
    fullNameLabel.append(spanLastName);;
    
    fullNameDiv.append(fullNameLabel);
    
    teacherCompactCard.append(fullNameDiv);
    
    const countryDiv = document.createElement("div");
    countryDiv.id = "countryOriginDiv";
    
    const countryLabel = document.createElement("label");
    countryLabel.id = "country";
    countryLabel.innerHTML = teacher.country;
    
    countryDiv.append(countryLabel);
    
    teacherCompactCard.append(countryDiv);
    displayTeachers.append(teacherCompactCard);
    
}

for(let i = 0;i<usersData.length;++i)
{
    DisplayCompactCardAtPanel(usersData[i], "topTeachersDisplayPanel");
}

//Filter parameters
const ageParam = document.getElementById("ageComboBox");
const regionParam = document.getElementById("regionComboBox");
const sexParam = document.getElementById("sexComboBox");
const withPhotoParam = document.getElementById("onlyPhotoCheckbox");
const favouritesParam = document.getElementById("onlyFavouritesCheckbox");

ageParam.addEventListener("change", function(){UpdateCurrentTeachers();DisplayCurrentTeachers();DisplayCharts()});
regionParam.addEventListener("change", function(){UpdateCurrentTeachers();DisplayCurrentTeachers();DisplayCharts()});
sexParam.addEventListener("change", function(){UpdateCurrentTeachers();DisplayCurrentTeachers();DisplayCharts()});
withPhotoParam.addEventListener("change", function(){UpdateCurrentTeachers();DisplayCurrentTeachers();DisplayCharts()});
favouritesParam.addEventListener("change", function(){UpdateCurrentTeachers();DisplayCurrentTeachers();DisplayCharts()});

function UpdateCurrentTeachers()
{
    let parameters = [];
    if(ageParam.value != "")
    {
        let ages = ageParam.value.split('-');
        let agesParsed = [parseInt(ages[0]), parseInt(ages[1])];
        parameters.push({key: "age", range: agesParsed, sat_cond: true, sat_all_cond: true});
    }
    if(regionParam.value != "")
    {
        parameters.push({key: "country", values: countriesToContinents[regionParam.value], sat_cond: true, sat_all_cond: false});
    }
    if(sexParam.value != "")
    {
        let sex = sexParam.value;
        parameters.push({key: "gender", values: [sex], sat_cond: true, sat_all_cond: true});
    }
    if(withPhotoParam.checked)
    {
        parameters.push({key: "picture_large", values: [undefined, null], sat_cond: false, sat_all_cond: true});
    }
    if(favouritesParam.checked)
    {
        parameters.push({key: "favourite", values: [true], sat_cond: true, sat_all_cond: true});
    }
    currentTeachers = FilterUsers(usersData, parameters);
}



var sortedUsers;

function UpdateTableStatsBasedOnParameters(parameters, isAsce = true)
{
    sortedUsers = [...currentTeachers];
    SortUsers(sortedUsers, parameters, isAsce);
}

Chart.register(ChartDataLabels);
var chartTeachersBySex = null;
var chartTeachersByAges = null;
var chartTeachersByRegion = null;
var specialtiesChart =  null;
function DisplayCharts()
{

    let bySexUserCount = [];
    let bySexUserPercentage = [];
    bySexUserCount.push(FilterUsers(currentTeachers,[{key: "gender", values: ["Male"], sat_cond: true, sat_all_cond: true}]).length);
    bySexUserCount.push(FilterUsers(currentTeachers,[{key: "gender", values: ["Female"], sat_cond: true, sat_all_cond: true}]).length);
    bySexUserPercentage.push(FilterUsersPercentage(currentTeachers,[{key: "gender", values: ["Male"], sat_cond: true, sat_all_cond: true}]));
    bySexUserPercentage.push(FilterUsersPercentage(currentTeachers,[{key: "gender", values: ["Female"], sat_cond: true, sat_all_cond: true}]));
    let currentSexIndex = 0;

    chartTeachersBySex?.destroy();

    chartTeachersBySex = new Chart("usersBySexChart",{
        type: "doughnut",
        data:{
            labels: ["Male","Female"],
            datasets:[{
                backgroundColor: ["blue", "red"],
                data: bySexUserCount
            }]
        },
        options: {

            tooltips: {
                enabled: true
              },
            plugins:{
                datalabels:{
                    formatter: function(value, context){
                        return bySexUserPercentage[currentSexIndex++].toFixed(2) + "%";
                    },
                    color:"#fff"
                },          
                title:{
                    display: true,
                    text: "Amount of teachers by sex"
                },
                legend:
                {
                    display: true
                }
            }
          }
    });

    let ageRanges = ["18-30","31-50","51-80"];
    let byAgesUserCount = [];
    let byAgesUserPercentage = [];
    let currentAgeIndex = 0;
    for(let i = 0;i<ageRanges.length;++i)
    {
        let agesString = ageRanges[i].split('-');
        let ages = [parseInt(agesString[0]),parseInt(agesString[1])]; 
        byAgesUserCount.push(FilterUsers(currentTeachers,[{key: "age", range: ages, sat_cond: true, sat_all_cond: true}]).length);
        byAgesUserPercentage.push(FilterUsersPercentage(currentTeachers,[{key: "age", range: ages, sat_cond: true, sat_all_cond: true}]));
    }

    chartTeachersByAges?.destroy();
    
    chartTeachersByAges = new Chart("usersByAgeChart",{
        type: "pie",
        data:{
            labels: ageRanges,
            datasets:[{
                backgroundColor: ["blue", "red", "green"],
                data: byAgesUserCount
            }]
        },
        options: {

            tooltips: {
                enabled: true
              },
            plugins:{
                datalabels:{
                    formatter: function(value, context){
                        return byAgesUserPercentage[currentAgeIndex++].toFixed(2) + "%";
                    },
                    color:"#fff"
                },
                title:{
                    display: true,
                    text: "Amount of teachers by age"
                },
                legend:
                {
                    display: true
                }
            }
          }
    });


    let regions = Object.keys(countriesToContinents);
    let byRegionUserCount = [];
    for(let i = 0;i<regions.length;++i)
    {
        byRegionUserCount.push(FilterUsers(currentTeachers,[{key: "country", values: countriesToContinents[regions[i]], sat_cond: true, sat_all_cond: false}]).length);
    }
    let barColors = ["red", "green","blue","orange","brown","yellow"];

    chartTeachersByRegion?.destroy();

    chartTeachersByRegion = new Chart("usersByRegionsChart",{
        type: "bar",
        data:{
            labels: regions,
            datasets:[{
                backgroundColor: barColors,
                data: byRegionUserCount
            }]
        },
        options: {
            plugins:{
                datalabels:{
                    display: false
                },         
                title:{
                    display: true,
                    text: "Amount of teachers by regions"
                },
                legend:
                {
                    display: false
                },
            }

        }
    });

    
    let byCourseUserCount = [];
    for(let i = 0 ;i<courses.length;++i)
    {
        byCourseUserCount.push(FilterUsers(currentTeachers, [{key: "course", values: [courses[i]], sat_cond: true, sat_all_cond: true}]).length);
    }
    specialtiesChart?.destroy();
    specialtiesChart = new Chart("specialtiesChart",{
        type: "bar",
        data:{
            labels: courses,
            datasets:[{
                backgroundColor: [],
                data: byCourseUserCount
            }]
        },
        options: {

            plugins:{
                datalabels:{
                    color: "#fff"
                },         
                title: {
                    display: true,
                    text: "Amount of teachers by specialty"
                  },
                legend:
                {
                    display: false
                }
            }
          }
    });
    
    let byAgeUserCount = [];

}


UpdateTableStatsBasedOnParameters([]);
DisplayCharts();


//Favourite teachers
var favUsers = FilterUsers(usersData,[{key: "favourite", values: [true], sat_cond: true, sat_all_cond: true}]);
var currentFavouriteUsersTab = 0;
var currentTeachersPerTabLimit = document.getElementById("fullWebsite").offsetWidth > 600 ? 6 : 5;
var previousWidth = document.getElementById("favouriteTeachers").offsetWidth;
var maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);

function ChangeTabCountFavourite()
{
    maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);
}

function UpdateFavouriteTeachers()
{
    favUsers = FavouriteTeachers();
    ChangeTabCountFavourite();

}

document.getElementById("nextTeacher").addEventListener("click", function(){
    if(currentFavouriteUsersTab<maxFavUsersTabs - 1)
    {
        ++currentFavouriteUsersTab;
        DisplayFavouriteTeachers();
    }
});
document.getElementById("prevTeacher").addEventListener("click", function(){
    if(currentFavouriteUsersTab > 0)
    {
        --currentFavouriteUsersTab;
        DisplayFavouriteTeachers();
    }
});

function ChangeFavUsersLimit()
{
    let pageWidth = document.getElementById("favouriteTeachers").offsetWidth;
    if(previousWidth == pageWidth)return false;
    previousWidth = pageWidth;
    currentTeachersPerTabLimit = Math.floor(pageWidth / 120); 
    maxFavUsersTabs = Math.ceil(favUsers.length / currentTeachersPerTabLimit);
    return true;
}

function OnResizeFavTeachersDisplay()
{
    if(ChangeFavUsersLimit()) DisplayFavouriteTeachers();
}

function debounce(func, delay)
{
    let timer;
    return (...args) =>
    {
        if(timer)clearTimeout(timer);
        timer = setTimeout(()=>
        {
            func(...args);
        }, delay);
    }
}


window.addEventListener("resize", debounce(function(e)
{
    OnResizeFavTeachersDisplay();
}, 200));

function FavouriteTeachers()
{
    return FilterUsers(usersData,[{key: "favourite", values: [true], sat_cond: true, sat_all_cond: true}]);
}

function DisplayFavouriteTeachers()
{
    RemoveAllChildren(document.getElementById("favouriteDisplayPanel"));
    for(let i = currentFavouriteUsersTab * currentTeachersPerTabLimit;i<Math.min(currentFavouriteUsersTab * currentTeachersPerTabLimit + currentTeachersPerTabLimit, favUsers.length);++i)
    {
        DisplayCompactCardAtPanel(favUsers[i], "favouriteDisplayPanel");
    }
}

DisplayFavouriteTeachers();

function DaysTillBirthday(date)
{
    let dateNow = dayjs();
    let birthdayDate = dayjs(date);
    birthdayDate = birthdayDate.year(dateNow.year());
    if(birthdayDate.isBefore(dateNow))
    {
        birthdayDate = birthdayDate.year(birthdayDate.year() + 1);
    }
    return birthdayDate.diff(dateNow,'day');
}